//
//  newItemViewController.swift
//  swiftLibrary
//
//  Created by Yifan Xiao on 8/1/15.
//  Copyright (c) 2015 Yifan Xiao. All rights reserved.
//

import UIKit
import SVProgressHUD


class newItemViewController: UIViewController {
    
    @IBOutlet weak var titleTextField: UITextField!
    
    @IBOutlet weak var authorTextField: UITextField!
    
    @IBOutlet weak var publisherTextField: UITextField!
    
    @IBOutlet weak var categoryTextField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func cancelButtonTapped(sender: UIButton) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func submitButtonTapped(sender: UIButton) {
        
        if !SharedNetworking.sharedInstance.hasNetworkAvailability() {
            var alert = UIAlertController(title: "Network Unavailable", message: "the network you are using is currently unavailable.", preferredStyle: UIAlertControllerStyle.Alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
        var newBook = book.new()
        
        let title = self.titleTextField.text
        let author = self.authorTextField.text
        
        if title.isEmpty || author.isEmpty {
            let alertController = UIAlertController(title: "Warning", message:
                "your name and author cannot be empty", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Done", style: UIAlertActionStyle.Default,handler: nil))
            
            self.presentViewController(alertController, animated: true, completion: nil)
        }
        else{
            SVProgressHUD.show()
            newBook.title = self.titleTextField.text
            newBook.author = self.authorTextField.text
            newBook.category = self.categoryTextField.text
            newBook.publisher = self.publisherTextField.text
            
            //sent new data to server
            SharedNetworking.sharedInstance.uploadData(newBook, completionHandler: { (response) -> Void in
                
                if response == true {
                    println("new data added")
                    SVProgressHUD.dismiss()
                    self.dismissViewControllerAnimated(true, completion: nil)
                }
                
            })
        }
        
    }


}
