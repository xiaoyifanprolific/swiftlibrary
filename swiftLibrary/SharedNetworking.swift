//
//  SharedNetworking.swift
//  librarySwift
//
//  Created by Yifan Xiao on 7/31/15.
//  Copyright © 2015 Yifan Xiao. All rights reserved.
//

import UIKit
import Alamofire
import SystemConfiguration
import Reachability

class SharedNetworking: NSObject {
    
    let prolificURL = "http://prolific-interview.herokuapp.com/55ba3cc5430a1d0009161084"
    
    static let sharedInstance = SharedNetworking()
    
    func hasNetworkAvailability ()-> Bool{
        let reachability: Reachability = Reachability.reachabilityForInternetConnection()
        let networkStatus: Int = reachability.currentReachabilityStatus().rawValue
        return networkStatus != 0
        
    }
    
    func getFeed(url: String, completionHandler : (dataArray : NSArray, response: Bool) -> Void ){
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        var taskURL = prolificURL.stringByAppendingString(url)
        
    
        Alamofire.request(.GET, taskURL)
            .response
            {
                (request, response, data, error) in
                
                let decodedJson: AnyObject? = NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments, error: nil)
                
                if let objects = decodedJson as? NSArray{
                    
                    if error == nil && response?.statusCode == 200{
                        completionHandler(dataArray: objects, response: true)
                    }
                    else{
                        completionHandler(dataArray: objects, response: false)
                    }
                    
                    UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                }
                
        }
        
    }
    
    
    func updateData(bookItem:book, completionHandler: (response: Bool) -> Void ){
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        
        let parameters = [
            "title":bookItem.title,
            "author":bookItem.author,
            "categories":bookItem.category,
            "publisher":bookItem.publisher
        ]
        
        var taskURL = prolificURL.stringByAppendingString(bookItem.url)
        Alamofire.request(.PUT, taskURL, parameters: parameters)
            .response{
                (request, response, data, error) in
                
                completionHandler(response: true)
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        }
        
    }
    
    func checkoutData(bookItem:book, completionHandler: (response: Bool) -> Void ){
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        
        let parameters = [
            "lastCheckedOutBy":bookItem.lastCheckedOutBy
        ]
        
        var taskURL = prolificURL.stringByAppendingString(bookItem.url)
        Alamofire.request(.PUT, taskURL, parameters: parameters)
            .response{
                (request, response, data, error) in
                
                completionHandler(response: true)
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        }
        
    }
    
    func uploadData(bookItem:book, completionHandler: (response: Bool) -> Void ){
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        let parameters = [
            "title":bookItem.title,
            "author":bookItem.author,
            "categories":bookItem.category,
            "publisher":bookItem.publisher
        ]
        
        var taskURL = prolificURL.stringByAppendingString("/books/")
        Alamofire.request(.POST, taskURL, parameters: parameters)
            .response{
                (request, response, data, error) in
                if error == nil && response?.statusCode == 200 {
                 completionHandler(response: true)
                }
                else{
                 completionHandler(response: false)
                }
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        }
        
    }
    
    
    func deleteBook(bookItem:book, completionHandler: (response: Bool) -> Void ){
    
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        var taskURL = prolificURL.stringByAppendingString(bookItem.url)
        Alamofire.request(.DELETE, taskURL)
            .response {
                (request, response, data, error) -> Void in
                if error == nil && (response?.statusCode == 200 || response?.statusCode == 204 || response?.statusCode == 202){
                    completionHandler(response: true)
                }
                else{
                    completionHandler(response: false)
                }
        }
    
    }
    
}
