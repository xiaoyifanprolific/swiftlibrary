//
//  DetailViewController.swift
//  swiftLibrary
//
//  Created by Yifan Xiao on 7/31/15.
//  Copyright (c) 2015 Yifan Xiao. All rights reserved.
//

import UIKit
import AHKActionSheet
import Social
import SVProgressHUD


class DetailViewController: UIViewController, UIAlertViewDelegate {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var authoerLabel: UILabel!

    @IBOutlet weak var categoryLabel: UILabel!
    
    @IBOutlet weak var publisherLabel: UILabel!
    
    @IBOutlet weak var lastCheckoutLabel: UILabel!
    
    
    var detailItem: book? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }

    func configureView() {
        // Update the user interface for the detail item.
        if let detail: book = self.detailItem {
            if let label = self.titleLabel {
                label.text = detail.title
            }
            if let label = self.authoerLabel {
                label.text = detail.author
            }
            if let label = self.categoryLabel {
                label.text = detail.category
            }
            if let label = self.publisherLabel {
                label.text = detail.publisher
            }
            if let label = self.lastCheckoutLabel {
                if detail.lastCheckedOut == nil{
                label.text = "nobody checked it out yet"
                }
                else{
                    
                let dateFormatter = NSDateFormatter.new()
                dateFormatter.timeZone = NSTimeZone(abbreviation: "GMT")
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let date = dateFormatter.dateFromString(detailItem!.lastCheckedOut as String)
                
                dateFormatter.dateFormat = "dd-MMM-YYYY HH:mm"
                dateFormatter.timeZone = NSTimeZone.localTimeZone()
                let localTimeStr = dateFormatter.stringFromDate(date!)
                    
                label.text = detailItem!.lastCheckedOutBy! + " @ " + localTimeStr
                }
                
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func updateTapped(sender: UIBarButtonItem) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier("updateVC") as! updateViewController
        
        vc.detailItem = detailItem
        
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    @IBAction func checkoutButtonTapped(sender: UIButton) {
        
        var alert = UIAlertView()
        alert.title = "Wanna check out this book?"
        alert.message = "Enter your name please"
        alert.delegate = self
        alert.addButtonWithTitle("Done")
        alert.alertViewStyle = UIAlertViewStyle.PlainTextInput
        alert.addButtonWithTitle("Cancel")
        alert.show()
        
    }
    
    //Pragma alertView delegate
    
    func alertView(alertView: UIAlertView, didDismissWithButtonIndex buttonIndex: Int) {
        
        if buttonIndex == 0{
            
            let textField:UITextField = alertView.textFieldAtIndex(0)!
            if let myText = textField.text {
                
                    if myText.isEmpty {
                        //show another alert
                        
                        let alertController = UIAlertController(title: "Warning", message:
                            "your name cannot be empty", preferredStyle: UIAlertControllerStyle.Alert)
                        alertController.addAction(UIAlertAction(title: "Done", style: UIAlertActionStyle.Default,handler: nil))
                        
                        self.presentViewController(alertController, animated: true, completion: nil)
                        
                    }
                    else{
                        
                        let dateFormatter = NSDateFormatter.new()
                        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                        let currentDate = NSDate.new()
                        let dateStr = dateFormatter.stringFromDate(currentDate)
                        //this is the date string to update
                        self.detailItem?.lastCheckedOutBy = myText
                        self.detailItem?.lastCheckedOut = dateStr
                        
                        dateFormatter.dateFormat = "dd-MMM-YYYY HH:mm"
                        let displayDateStr = dateFormatter.stringFromDate(currentDate)
                        
                        self.lastCheckoutLabel.text = myText + " @ " + displayDateStr
                        
                        SVProgressHUD.show()
                        //update data
                        SharedNetworking.sharedInstance.checkoutData(self.detailItem!, completionHandler: { (response) -> Void in
                            //the other code
                            if response == true {
                                println("succeeded")
                                SVProgressHUD.dismiss()
                            }
                        })
                    }
                
            }
        }
        
    }

    
    @IBAction func shareBookTapped(sender: AnyObject) {
        
        let actionSheet = AHKActionSheet.new()
        actionSheet.title = "Wanna share this item to friends?"
        
        actionSheet.addButtonWithTitle("Facebook", image: UIImage(named: "facebook"), type: AHKActionSheetButtonType.Default) { (AHKActionSheet) -> Void in
            
            if SLComposeViewController.isAvailableForServiceType(SLServiceTypeFacebook){
                
                if let facebookPost:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook){
                 
                    self.presentViewController(facebookPost, animated: true, completion: nil)
                    
                }
                
            }
            else
            {
                
                var alert = UIAlertController(title: "Accounts", message: "Please login to a Facebook account to share.", preferredStyle: UIAlertControllerStyle.Alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                
                
            }
        }
        
        
        actionSheet.addButtonWithTitle("Twitter", image: UIImage(named: "twitter"), type: AHKActionSheetButtonType.Default) { (AHKActionSheet) -> Void in
            
            if SLComposeViewController.isAvailableForServiceType(SLServiceTypeTwitter){
                
                if let twitterPost:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeTwitter){
                    
                    self.presentViewController(twitterPost, animated: true, completion: nil)
                    
                }
                
            }
            else
            {
                
                var alert = UIAlertController(title: "Accounts", message: "Please login to a Twitter account to share.", preferredStyle: UIAlertControllerStyle.Alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                
                
            }
        }
        
        actionSheet.addButtonWithTitle("Weibo", image: UIImage(named: "weibo"), type: AHKActionSheetButtonType.Default) { (AHKActionSheet) -> Void in
            
            if SLComposeViewController.isAvailableForServiceType(SLServiceTypeSinaWeibo){
                
                if let weiboPost:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeSinaWeibo){
                    
                    self.presentViewController(weiboPost, animated: true, completion: nil)
                    
                }
                
            }
            else
            {
                
                var alert = UIAlertController(title: "Accounts", message: "Please login to a Weibo account to share.", preferredStyle: UIAlertControllerStyle.Alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                
                
            }
        }
        
        actionSheet.show()
        
    }
    

}

