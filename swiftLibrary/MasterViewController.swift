//
//  MasterViewController.swift
//  swiftLibrary
//
//  Created by Yifan Xiao on 7/31/15.
//  Copyright (c) 2015 Yifan Xiao. All rights reserved.
//

import UIKit
import SVProgressHUD

class MasterViewController: UITableViewController {

    var detailViewController: DetailViewController? = nil
    var objects = [AnyObject]()
    var dataArray = [book]()

    override func awakeFromNib() {
        super.awakeFromNib()
        if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
            self.clearsSelectionOnViewWillAppear = false
            self.preferredContentSize = CGSize(width: 320.0, height: 600.0)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        if let split = self.splitViewController {
            let controllers = split.viewControllers
            self.detailViewController = controllers[controllers.count-1].topViewController as? DetailViewController
        }
        
        let pullToFresh = UIRefreshControl.new()
        pullToFresh.addTarget(self, action:"refreshAction" , forControlEvents: UIControlEvents.ValueChanged)
        self.refreshControl = pullToFresh
    }
    
    func refreshAction(){
    
        self.loadDataFromServer()
        self.tableView.reloadData()
        self.refreshControl?.endRefreshing()
        
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if !SharedNetworking.sharedInstance.hasNetworkAvailability(){
            var alert = UIAlertController(title: "Network Unavailable", message: "the network you are using is currently unavailable.", preferredStyle: UIAlertControllerStyle.Alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else{
            self.loadDataFromServer()
        }
        
        self.tableView.estimatedRowHeight = 130;
        self.tableView.rowHeight = UITableViewAutomaticDimension;
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Segues

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showDetail" {
            if let indexPath = self.tableView.indexPathForSelectedRow() {
                let object = self.dataArray[indexPath.row]
                let controller = (segue.destinationViewController as! UINavigationController).topViewController as! DetailViewController
                controller.detailItem = object
                controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }

    // MARK: - Table View

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("mainCell", forIndexPath: indexPath) as! mainTableCell
        let object = self.dataArray[indexPath.row] as book
        
        cell.title.text = object.title
        cell.category.text = object.category
        cell.author.text = object.author
        
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        return cell
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension;
    }
    
    override func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension;
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        let bookItem = self.dataArray[indexPath.row] as book
        
            SVProgressHUD.show()
            SharedNetworking.sharedInstance.deleteBook(bookItem, completionHandler: { (response) -> Void in
                self.dataArray.removeAtIndex(indexPath.row)
                tableView.beginUpdates()
                tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
                tableView.endUpdates()
                SVProgressHUD.dismiss()
            })
        
        
    }
    
    func loadDataFromServer(){
        SVProgressHUD.show()
        SharedNetworking.sharedInstance.getFeed("/books", completionHandler: { (dataArray, response) -> Void in
            
            self.objects = dataArray as [(AnyObject)];
            
            self.dataArray.removeAll(keepCapacity: false)
            
            for obj in self.objects {
                let book = self.castToBook(obj)
                self.dataArray.append(book)
            }
            self.tableView.reloadData()
            SVProgressHUD.dismiss()
        })
    }
    
    func castToBook(bookObj:AnyObject) -> book{
        
        var newBook = book.new()
        if let dict = bookObj as? NSDictionary{
            
            if let title = dict["title"] as? String{
                newBook.title = title
            }
            
            if let author = dict["author"] as? String{
                newBook.author = author
            }
            
            if let category = dict["categories"] as? String{
                newBook.category = category
            }
            
            if let url = dict["url"] as? String{
                newBook.url = url
            }
            
            if let lastCheckedOut = dict["lastCheckedOut"] as? String{
                newBook.lastCheckedOut = lastCheckedOut
            }
            
            if let lastCheckedOutBy = dict["lastCheckedOutBy"] as? String{
                newBook.lastCheckedOutBy = lastCheckedOutBy
            }
            
            if let ID = dict["id"] as? String{
                newBook.ID = ID
            }
            
            if let publisher = dict["publisher"] as? String{
                newBook.publisher = publisher
            }
        }
        
        return newBook
    }


}

