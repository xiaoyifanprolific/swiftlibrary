//
//  mainTableCell.swift
//  swiftLibrary
//
//  Created by Yifan Xiao on 7/31/15.
//  Copyright (c) 2015 Yifan Xiao. All rights reserved.
//

import UIKit

class mainTableCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var author: UILabel!
    
    @IBOutlet weak var category: UILabel!
    
}
