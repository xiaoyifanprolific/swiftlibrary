//
//  book.swift
//  librarySwift
//
//  Created by Yifan Xiao on 7/31/15.
//  Copyright © 2015 Yifan Xiao. All rights reserved.
//

import UIKit

class book: NSObject, NSCoding {
    
    var title:String!
    var category:String!
    var author:String!
    var url:String!
    var ID:String!
    var publisher:String!
    var lastCheckedOut:String!
    var lastCheckedOutBy:String!
    
    required convenience init(coder decoder:NSCoder){
        self.init()
        self.title = decoder.decodeObjectForKey("title") as! String?
        self.category = decoder.decodeObjectForKey("category") as! String
        self.author = decoder.decodeObjectForKey("author") as! String
        self.url = decoder.decodeObjectForKey("url") as! String
        self.ID = decoder.decodeObjectForKey("ID") as! String
        self.publisher = decoder.decodeObjectForKey("publisher") as! String
        self.lastCheckedOut = decoder.decodeObjectForKey("lastCheckedOut") as! String
        self.lastCheckedOutBy = decoder.decodeObjectForKey("lastCheckedOutBy") as! String
        
    }
    
    func encodeWithCoder(coder: NSCoder) {
        coder.encodeObject(self.title, forKey: "title")
        coder.encodeObject(self.author, forKey: "author")
        coder.encodeObject(self.category, forKey: "pageCount")
        coder.encodeObject(self.url, forKey: "categories")
        coder.encodeObject(self.ID, forKey: "available")
        coder.encodeObject(self.publisher, forKey: "publisher")
        coder.encodeObject(self.lastCheckedOut, forKey:"lastCheckedOut")
        coder.encodeObject(self.lastCheckedOutBy, forKey: "lastCheckedOutBy")
    }

}
