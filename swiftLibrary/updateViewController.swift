//
//  updateViewController.swift
//  swiftLibrary
//
//  Created by Yifan Xiao on 8/1/15.
//  Copyright (c) 2015 Yifan Xiao. All rights reserved.
//

import UIKit
import SVProgressHUD

class updateViewController: UIViewController {
    
    @IBOutlet weak var titleField: UITextField!
    
    @IBOutlet weak var authorField: UITextField!
    
    @IBOutlet weak var publisherField: UITextField!
    
    @IBOutlet weak var categoryField: UITextField!
    
    
    @IBAction func updateTapped(sender: UIButton) {
        
        if !SharedNetworking.sharedInstance.hasNetworkAvailability(){
            var alert = UIAlertController(title: "Network Unavailable", message: "the network you are using is currently unavailable.", preferredStyle: UIAlertControllerStyle.Alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else
        {
            detailItem?.title = self.titleField.text
            detailItem?.author = self.authorField.text
            detailItem?.publisher = self.publisherField.text
            detailItem?.category = self.categoryField.text
        
        SVProgressHUD.show()
        //update the book info here
        SharedNetworking.sharedInstance.updateData(self.detailItem!, completionHandler: { (response) -> Void in
            //the other code
            if response == true {
                println("succeeded")
                SVProgressHUD.dismiss()
                self.dismissViewControllerAnimated(true, completion: nil)
                
            }
        })
    }
    
    }
    
    @IBAction func cancelTapped(sender: UIButton) {
    
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    

    var detailItem: book? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }
    
    func configureView() {
        // Update the user interface for the detail item.
        if let detail: book = self.detailItem {
            if let label: UITextField = self.titleField {
                label.text = detail.title
            }
            if let label: UITextField = self.authorField {
                label.text = detail.author
            }
            if let label: UITextField = self.publisherField {
                label.text = detail.publisher
            }
            if let label: UITextField = self.categoryField {
                label.text = detail.category
            }
        }
    }
    
    override func viewDidLoad() {
        self.configureView()
    }
    
}
